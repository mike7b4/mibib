use std::path::PathBuf;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::mem::size_of;
use structopt::StructOpt;
#[repr(C)]
#[derive(Debug)]
struct MibibHeader {
    magic1: u32,
    magic2: u32,
    version: u32,
    age: u32,
}

impl Default for MibibHeader {
    fn default() -> Self {
        Self {
            magic1: 0xFE569FAC,
            magic2: 0xCD7F127A,
            version: 4,
            age: 0,
        }
    }
}

impl MibibHeader {
    fn new() -> Self {
        Self {
            magic1: 0,
            magic2: 0,
            version: 0,
            age: 0,
        }
    }
}

impl PartialEq for MibibHeader {
    fn eq(&self, other: &Self) -> bool {
        if self.magic1 == other.magic1 {
            return false;
        }
        if self.magic2 == other.magic2 {
            return false;
        }
        true
    }
}

#[derive(StructOpt)]
struct Args {
    mibibfile: PathBuf,
}

fn main() {
    let args = Args::from_args();
    let eq = MibibHeader::default();
    let mut mibib = MibibHeader::new();
    let mut ptr = unsafe {
        std::slice::from_raw_parts_mut(&mut mibib as *mut MibibHeader as *mut u8, size_of::<MibibHeader >())

    };

    let mut file = OpenOptions::new().read(true).open(&args.mibibfile).expect("expect"); 
    if file.read(&mut ptr).expect("read bailed") != size_of::<MibibHeader>() {
        panic!("bailed");
    }
    println!("{:X?}", mibib);
    println!("{:X?}", eq);
}
